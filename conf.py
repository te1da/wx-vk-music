#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from re import findall, sub
from ConfigParser import SafeConfigParser

# Config parser
prs = SafeConfigParser()

def get_conf_dir():
    """ Return path to directory where will be saved app's config """ 
    
    conf_dir = ""
    
    # If Linux
    # App save config in XDG_CONFIG_HOME dir from XDG Base Directory spec
    # http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
    if os.name == "posix":
        if os.getenv("XDG_CONFIG_HOME"):
            conf_dir = os.path.join(os.getenv("XDG_CONFIG_HOME"), "wxvkmusic")
        else:
            conf_dir = os.path.join(os.path.expanduser("~"), 
                                    ".config/wxvkmusic")
    # If windows: Documents\wxvkmusic
    else:
        conf_dir = os.path.join(os.path.expanduser("~"), "wxvkmusic")
    
    # Make dir
    if not os.path.exists(conf_dir):
        os.mkdir(conf_dir)
    
    return conf_dir
    
def read_conf():
    """ Read configuration from file """
    
    f = os.path.join(get_conf_dir(), "config.ini")  # config's path
    
    count = None    
    
    if not os.path.exists(f):
        # Write to file
        with open(f, "w") as config_file:         
            config_file.write("[app]")
            config_file.write("")
    else:
        # Update sections nums
        count, data = update_sec_nums(f)
        with open(f, "w") as config_file:
            config_file.write(data)
        
    # Read
    prs.read(f)  
    
    if count:
        prs.set("app", "playlists", str(count))

def save_conf():
    """ Save changes to configuration file """
    
    f = os.path.join(get_conf_dir(), "config.ini")  # config's path
    
    # Write to file
    with open(f, "w") as config_file:
        prs.write(config_file)

def save_token(t):
    """ Save access token to config """
    
    prs.set("app", "access_token", t)
    
def get_token():
    """ Get access token from config """
    
    if prs.has_option("app", "access_token"):
        return prs.get("app", "access_token")
    else:
        return False

def save_login(l):
    """ Save user's login """
    
    prs.set("app", "login", l)
    
def get_login():
    """ Get user's login from config """
    
    if prs.has_option("app", "login"):
        return prs.get("app", "login")
    else:
        return False
        
def get_playlists_num():
    """ Get number of added playlists from config """
    
    if prs.has_option("app", "playlists"):
        return prs.getint("app", "playlists")
    else:
        prs.set("app", "playlists", "0")
        return 0

def get_playlists():
    """ Get list with playlists sections names """
    
    return prs.sections()
    
def get_playlist_info(s):
    """ Get details about playlist from config """
    
    name = prs.get(s, "name")
    _id = prs.get(s, "id")
    pos = prs.getint(s, "pos")
    
    if prs.has_option(s, "album"):  # if has album
        album = prs.get(s, "album")
    else:
        album = False
    
    # Return dict
    return {"name": name, "id": _id, "pos": pos, "album": album}
    
def get_playlist_section(n):
    """ Get name of playlist's section in config by page's name """
    
    for sec in prs.sections():
        if not sec == "app" and prs.get(sec, "name") == n:
            return sec
        
def add_playlist(name, i, pos, album=False):
    """ Add new playlist to config """
    
    # Number of already added playlists
    n = get_playlists_num()
        
    # Section name
    sec = "playlist" + str(n)
    
    # Add new section about new playlist
    prs.add_section(sec)
    
    prs.set(sec, "name", name)  # playlist name
    prs.set(sec, "id", i)  # playlist id
    prs.set(sec, "pos", str(pos))  # playlist pos
    
    if album:
        prs.set(sec, "album", album)  # album id
    
    # +1 to app - playlists
    prs.set("app", "playlists", str(n + 1))
    
def rm_playlist(sec_name):
    """ Remove playlist from config """
    
    # Remove section
    prs.remove_section(sec_name)

def save_params(vol, tab, min_close, icons, ddir):
    """ Save app params """
    
    prs.set("app", "volume", str(vol))  # volume level
    prs.set("app", "last_tab", str(tab))  # last opened tab
    prs.set("app", "minimize_on_close", str(min_close))  # last opened tab
    prs.set("app", "sys_gtk_icons", str(icons))  # use sys gtk icons
    prs.set("app", "download_dir", ddir)
    
def get_params():
    """ Get app params """
    
    # Volume
    if prs.has_option("app", "volume"):
        vol = prs.getint("app", "volume")
    else:
        vol = 100
    
    # Last tab
    if prs.has_option("app", "last_tab"):
        # +1 because 0 == False on the if else block at ui.load_params
        tab = prs.getint("app", "last_tab") + 1 
    else:
        tab = False
        
    # Minize app window on close
    if prs.has_option("app", "minimize_on_close"):
        min_close = prs.getboolean("app", "minimize_on_close")
    else:
        min_close = False
        
    # Use system gtk icons
    if prs.has_option("app", "sys_gtk_icons"):
        icons = prs.getboolean("app", "sys_gtk_icons")
    else:
        icons = False
        
    # Download dir
    if prs.has_option("app", "download_dir"):
        ddir = prs.get("app", "download_dir")
    else:
        ddir = os.path.join(os.path.expanduser("~"), "Downloads")
        
    return {"vol": vol, "tab": tab, "min_close": min_close, "icons": icons, 
            "ddir": ddir}

def update_sec_nums(f):
    
    # Read
    with open(f, "r") as conf_file:
        data = conf_file.read()
        
        # Find added sections
        secs = findall("playlist\d+", data)
        
        count = 0 
        for i in secs:
            # Replace number in section name
            data = sub(i, "playlist" + str(count), data)
            count += 1
    
    return count, data
        