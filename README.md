# wx VK Music

См. описание на русском языке в README_ru.md

## About
wx VK Music is Free & OpenSource music player for [vk.com](https://vk.com) social network. 

## Feautures

* Add playlist by user/community ids 

* Add albums

* Tabs with playlists

* Playlist sorting

* Repeat audiotrack

* Play one random audiotrack

* Reverse playlist playing

* Random playlist playing

* App get permissions only to audio

* Optional: App can save login and session (access token)

* Passwords are NOT saved

* App configuration saves in ini config file. On GNU/Linux config file located in ~/.config/wxvkmusic/config.ini ($XDG_CONFIG_HOME/wxvkmusic/config.ini). See details about XDG_CONFIG_HOME in [XDG Base Directory specification](http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html).

## Dependencies

* [Python 2](https://www.python.org/)

* [wxPython](http://wxpython.org/) & [wxWidgets](http://wxwidgets.org/)

* [vk](https://github.com/dimka665/vk) module

* [requests](http://docs.python-requests.org/en/latest/) module

## Additional info

See list of planned features in TODO.md

Licensed under GNU GPL v3 or newer. See COPYING for details. 

Oleg Kozlov (xxblx), 2014 - 2015