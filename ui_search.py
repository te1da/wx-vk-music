#!/usr/bin/python
# -*- coding: utf-8 -*-

import wx
from data import search_audios

_ = wx.GetTranslation

class search_dialog(wx.Dialog):
    """ Dialog: Search audiotracks """
    
    def __init__(self, *args, **kwargs):
        super(search_dialog, self).__init__(*args, **kwargs)
        self.init_ui()
    
    def init_ui(self):
        """ Load interface """
        
        self.SetTitle(_("Search"))
        self.make_layout()               
        size = wx.Size(250, self.GetBestSize()[1])
        self.SetSize(size)
        self.btn.Bind(wx.EVT_BUTTON, self.get_info, self.btn)
        
    def make_layout(self):
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        # --- Static text --- #
        st_text = wx.StaticText(self, wx.ID_ANY, _("Search"), 
                                wx.DefaultPosition, wx.DefaultSize, 0)
        st_text.Wrap(-1)
        
        # --- Text ctrl --- #
        self.ctrl = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, 
                                wx.DefaultPosition, wx.DefaultSize, 0)
                                
        # --- Button --- #
        self.btn = wx.Button(self, wx.ID_ANY, _("Search"), 
                             wx.DefaultPosition, wx.DefaultSize, 0)
                             
        sizer.AddMany([
            (st_text, 0, wx.ALIGN_CENTER | wx.ALL, 5),
            (self.ctrl, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 3),
            (self.btn, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        ])
        
        self.SetSizer(sizer)
        self.Layout()
    def get_info(self, event):
        """ Get information about audios """
        
        # Get query
        self.query = self.ctrl.GetValue()
        
        if not self.query or self.query == "":
            # If query not entered show error
            wx.MessageBox(_("You need to enter query"), 
                          _("Error"), wx.OK | wx.ICON_ERROR)
            event.Skip()
        else:

            # Get audios
            self.audios = search_audios(self.query)
            
            if not self.audios:
                wx.MessageBox(_("No audios found"), 
                              _("Error"), wx.OK | wx.ICON_ERROR)
            else:
                print self.audios #Print result to stdout (temporarily, for debug purpose)
                # Close dialog at the end
                self.Destroy()
