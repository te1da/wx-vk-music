#!/usr/bin/python
# -*- coding: utf-8 -*-

import wx

_ = wx.GetTranslation

class options_dialog(wx.Dialog):
    """ Options dialog """
    
    def __init__(self, *args, **kwargs):
        super(options_dialog, self).__init__(*args, **kwargs)
        self.init_ui()
        
    def init_ui(self):
        
        self.SetTitle(_("Options"))
        self.make_layout()
        self.SetInitialSize()
        
        # Events
        self.button.Bind(wx.EVT_BUTTON, self.get_info)
        self.Bind(wx.EVT_CLOSE, self.quit_dial)
        
    def make_layout(self):
        
        # Sizer
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        # --- Static text --- #
        # Download dir
        dir_text = wx.StaticText(self, wx.ID_ANY, _("Download directory"), 
                                 wx.DefaultPosition, wx.DefaultSize, 0)
        dir_text.Wrap(-1)
        
        # --- DirPickerCtrl --- #
        self.dir_picker = wx.DirPickerCtrl(self, wx.ID_ANY, wx.EmptyString, 
                                           _("Select a folder"), 
                                           wx.DefaultPosition, wx.DefaultSize, 
                                           wx.DIRP_DEFAULT_STYLE)
        
        # --- Checkboxes --- #
        self.min_close_cb = wx.CheckBox(self, wx.ID_ANY, 
                                        _("Minimize on close"), 
                                        wx.DefaultPosition, 
                                        wx.DefaultSize, 0)                              
        
        self.sys_icons_cb = wx.CheckBox(self, wx.ID_ANY, 
                                        _("Use system gtk icons"),
                                        wx.DefaultPosition,
                                        wx.DefaultSize, 0)
        
        if not wx.Platform  == '__WXGTK__':
            self.sys_icons_cb.Enable(False)
        
        # --- Button --- #
        self.button = wx.Button(self, wx.ID_ANY, _("Save changes"), 
                                wx.DefaultPosition, wx.DefaultSize, 0)
        
        sizer.AddMany([
            (dir_text, 0, wx.ALIGN_CENTER | wx.ALL, 6),
            (self.dir_picker, 0, wx.ALIGN_CENTER | wx.ALL, 0),
            (self.min_close_cb, 0, wx.ALIGN_CENTER | wx.ALL, 5),
            (self.sys_icons_cb, 0, wx.ALIGN_CENTER | wx.ALL, 5),
            (self.button, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        ])        
        
        self.SetSizer(sizer)
        self.Layout()
        
    def get_info(self, event):
        """ Get values from dialog widgets """
        
        self.values = {}
        self.values["min_close"] = self.min_close_cb.GetValue()
        self.values["icons"] = self.sys_icons_cb.GetValue()
        self.values["ddir"] = self.dir_picker.GetPath()
        
        self.Destroy()
        
    def quit_dial(self, event):
        """ Close dialog """
        
        self.values = False
        self.Destroy()
